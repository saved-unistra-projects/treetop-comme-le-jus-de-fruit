#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

#define DEFAULT_COLOR "yellow"
#define MAX_COLOR 16

#define MAKEFILE_PATH "/usr/share/treetop/Makefile"
#define TESTS_PATH "/usr/share/treetop/tests.h"

#define MAIN_CONTENT "#include <stdio.h>\n\nint main() {\nprintf(\"Hello World!\");\n}"

#define UPDATE_COMMAND "mkdir -p /usr/share/treetop;wget raphi.achencraft.fr/treetop/Makefile -O /usr/share/treetop/Makefile;wget raphi.achencraft.fr/treetop/tests.h -O /usr/share/treetop/tests.h"

#define SLOW_BLINK 5

int main(int argc, char **argv){
  char color[MAX_COLOR] = DEFAULT_COLOR;
  char path[PATH_MAX] = ".";
  char make[PATH_MAX] = "";
  int tests = 1;
  int color_number;
  int blink = 0;
  char c;
  int opt, update=0;


  while(( opt = getopt(argc, argv, "ubnc:"))!=-1){
    switch(opt) {
      case 'n' :{
		  tests = 0;
		  break;
		}
      case 'c' :{
		  strcpy(color,optarg);
		  break;
		}
      case 'b' :{
		  blink=1;
		  break;
		}
      case 'u' :{
		  update = 1;
		  break;
		}
      default :{
		 fprintf(stderr,"Usage : %s [-b] [-c color] [-n] path\n", argv[0]);
		 exit(EXIT_FAILURE);
	       }
    }
  }

  if(update){
    if(system(UPDATE_COMMAND)!=0)
      fprintf(stderr,"Update failed (maybe try with sudo)\n");
    else
      printf("Update Successful\n");
    exit(EXIT_SUCCESS);
  }

  if(optind < argc){
    strcpy(path,argv[optind]);
  }

  int test = snprintf(make,PATH_MAX,"%s/%s",path,"Makefile");

  if(test<0 || test>=PATH_MAX){
    fprintf(stderr,"snprintf");
    exit(EXIT_FAILURE);
  }

  FILE * stream, *stream2;

  stream = fopen(MAKEFILE_PATH,"r");

  stream2 = fopen(make,"w");

  if(stream == NULL){ 
    perror("Opening Makefile source");
    exit(EXIT_FAILURE);
  }
  
  if(stream2 == NULL){
    perror("Opening Makefile destination");
    exit(EXIT_FAILURE);
  }

  
  switch(color[0]){
    case 'b':{
	       if(color+2!=NULL && color[2]=='a'){
		 color_number = 30 ;
	       }
	       else if(color+2!=NULL && color[2]=='u'){
		 color_number = 34 ;
	       }
	       else{
		 fprintf(stderr,"Invalid color option\n");
		 exit(EXIT_FAILURE);
	       }
	       break;
	     }
    case 'r':{
	       color_number = 31 ;
	       break;
	     }
    case 'g':{
	       color_number = 32 ;
	       break;
	     }
    case 'y':{
	       color_number = 33 ;
	       break;
	     }
    case 'm':{
	       color_number = 35 ;
	       break;
	     }
    case 'c':{
	       color_number = 36 ;
	       break;
	     }
    case 'w':{
	       color_number = 37 ;
	       break;
	     }
    default :{
	       fprintf(stderr,"Invalid color option\n");
	       exit(EXIT_FAILURE);
	     }
  }

  if(!blink)
    fprintf(stream2,"COLOR_NUMBER = %d\n",color_number);
  else
    fprintf(stream2,"COLOR_NUMBER = %d;%d\n", color_number, SLOW_BLINK);

  while((c=fgetc(stream))!=EOF)
    fputc(c,stream2);

  char buf[PATH_MAX+8];

  if(tests){
    test = snprintf(buf,PATH_MAX+8,"mkdir -p %s/tests",path);
    system(buf);
    if(test<0 || test>=PATH_MAX+8){
      fprintf(stderr,"snprintf\n");
      exit(EXIT_FAILURE);
    }
    char t[PATH_MAX] = "tests.h";
    test = snprintf(t,PATH_MAX,"%s/tests/%s",path,"tests.h");
    if(test<0 || test >= PATH_MAX){
      fprintf(stderr,"snprintf\n");
      exit(EXIT_FAILURE);
    }
    stream = fopen(TESTS_PATH,"r");

    stream2 = fopen(t,"w");

    if(stream == NULL || stream2 == NULL){
      perror("Opening Makefile files");
      exit(EXIT_FAILURE);
    }

    while((c=fgetc(stream))!=EOF)
      fputc(c,stream2);
  }
  test = snprintf(buf,PATH_MAX+8,"mkdir -p %s/src",path);
  if(test<0 || test>=PATH_MAX+8){
    fprintf(stderr,"snprintf\n");
    exit(EXIT_FAILURE);
  }
  system(buf);
  test = snprintf(buf,PATH_MAX+8,"mkdir -p %s/include",path);
  if(test<0 || test>=PATH_MAX+8){
    fprintf(stderr,"snprintf\n");
    exit(EXIT_FAILURE);
  }
  system(buf);
  test = snprintf(buf,PATH_MAX+8,"mkdir -p %s/obj",path);
  if(test<0 || test>=PATH_MAX+8){
    fprintf(stderr,"snprintf\n");
    exit(EXIT_FAILURE);
  }
  system(buf);
  test = snprintf(buf,PATH_MAX+8,"%s/src/main.c",path);
  if(test<0 || test>=PATH_MAX+8){
    fprintf(stderr,"snprintf\n");
    exit(EXIT_FAILURE);
  }

  
  if(access(buf,F_OK)==-1){
    if(errno==ENOENT){
      stream = fopen(buf,"w");
      if(stream == NULL){
	perror("fopen");
	exit(EXIT_FAILURE);
      }
      fprintf(stream,MAIN_CONTENT);
    }
    else{
      perror("access");
      exit(EXIT_FAILURE);
    }
  }


  if(fclose(stream)==EOF){
    perror("fclose");
    exit(EXIT_FAILURE);
  }
}


