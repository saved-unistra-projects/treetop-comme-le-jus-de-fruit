# treetop (comme la marque de jus de fruits)

A simple command that sets up a c project.

## Installation

Run the following command :

```
sudo make && make install
```

## Usage

To run the program, simply run :

```
treetop
```

You can specify a path for the root of your project, this command sets the
project up in the `foo` directory on the Desktop :

```
treetop ~/Desktop/foo
```

## Options

You can run the program with the following options :

- `[-c color]̀`
Makes the color of the Makefile's output of the `color` argument.
The color must be written in lower case. 
Valid colors are :
- black
- red
- green
- yellow
- blue
- magenta
- cyan
- white

- `[-b]`
Makes the colors blink! (depends on your terminal, might not work)

- `[-u]`
Updates the makefile and test header

- `[-n]`
Do not import test header




