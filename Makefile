# Directories
O_DIR = obj/
EXEC_DIR = bin/
SRC_DIR = src/
HEADERS_DIR = include/
TEST_DIR = tests/

# Compilation
CC = gcc
CFLAGS = -g -Wall -Wextra -Werror


SOURCES = $(wildcard $(SRC_DIR)*.c)
OBJECTS = $(patsubst $(SRC_DIR)%.c, $(O_DIR)%.o, $(SOURCES))
TESTSRC = $(wildcard $(TEST_DIR)*.c)
TESTEXEC = $(patsubst $(TEST_DIR)%.c, $(TEST_DIR)%.test, $(TESTSRC))
TESTS = $(patsubst $(TEST_DIR)%.test, %.test, $(TESTEXEC))

# Executable name and main source file name
EXEC = main
EXEC_NAME = treetop


OBJECTSNOEXEC := $(filter-out $(O_DIR)$(EXEC).o, $(OBJECTS))


INIT_DIR = @mkdir -p

# Colors
COLOR = \033[1;$(COLOR_NUMBER)m
ENDCOLOR = \033[0m

vpath %.c src
vpath %.o obj
vpath %.h include
vpath main bin

# make all
all : $(EXEC_DIR)$(EXEC_NAME) $(TESTEXEC)

# make and run the program
exec: $(EXEC_DIR)$(EXEC_NAME)
	@$(EXEC_DIR)$(EXEC_NAME)

# check for memory leaks 
memcheck: $(EXEC_DIR)$(EXEC_NAME)

# Compile executable from objects
$(EXEC_DIR)$(EXEC_NAME) : $(OBJECTS)
	@echo "$(COLOR)------------------------Linking$(ENDCOLOR)"
	$(INIT_DIR) $(EXEC_DIR)
	$(CC) $(CFLAGS) -o $@ $^

# Compile main object
$(O_DIR)$(EXEC).o : $(SRC_DIR)$(EXEC).c
	@echo "$(COLOR)------------------------Compiling $@$(ENDCOLOR)"
	$(INIT_DIR) $(O_DIR)
	$(CC) $(CFLAGS) -I $(HEADERS_DIR) -c  $< -o $@

# Compile objects
$(O_DIR)%.o : $(SRC_DIR)%.c $(HEADERS_DIR)%.h
	@echo "$(COLOR)------------------------Compiling $@$(ENDCOLOR)"
	$(INIT_DIR) $(O_DIR)
	$(CC) $(CFLAGS) -I $(HEADERS_DIR) -c $< -o $@

# Tests

# Run all tests
test: $(TESTS)

# Run a specific test
%.test: $(TEST_DIR)%.test
	@echo "$(COLOR)------------------------Testing $@$(ENDCOLOR)"
	$(TEST_DIR)$@

# Compile tests
$(TEST_DIR)%.test: $(TEST_DIR)%.c $(OBJECTSNOEXEC)
	@echo "$(COLOR)------------------------Compiling test $@$(ENDCOLOR)"
	$(CC) $(CLFAGS) -I $(HEADERS_DIR) -I $(TEST_DIR) -o $@ $< $(OBJECTSNOEXEC)

# Clean and memcheck
memcheck : $(EXEC_DIR)$(EXEC_NAME)
	@echo "$(COLOR) ------------------------Stating memory leak check for $(EXEC)$(ENDCOLOR)"
	valgrind --leak-check=full $<


clean :
	@echo  "$(COLOR)------------------------Cleaning$(ENDCOLOR)"
	rm $(OBJECTS) $(EXEC_DIR)$(EXEC_NAME) $(TEST_DIR)*.test

#install

install :
	@cp $(EXEC_DIR)$(EXEC_NAME) /usr/bin
	@$(EXEC_DIR)$(EXEC_NAME) -u
	@cp doc/treetop.1 /usr/local/share/man/man1/treetop.1
	@gzip /usr/local/man/man1/treetop.1
